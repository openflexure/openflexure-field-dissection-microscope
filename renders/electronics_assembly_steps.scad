use <./stand_assembly.scad>

FRAME=3;
render_electronics_assembly_steps(FRAME);

module render_electronics_assembly_steps(frame){
    if (frame == 1){
        pi_tray_assembly(pi=true, pi_screws=false, plate=false, explode="pi");
        translate([-60, -105]){
            pi_tray_assembly(pi=true, pi_screws=true, plate=false, explode="pi_screws");
        }
        translate([-110, -190]){
            pi_tray_assembly(pi=true, plate=false);
        }
    }
    if (frame == 2){
        pi_tray_assembly(pi=true, pi_screws=true, plate=true, explode="plate");
        translate([-20, -150]){
            pi_tray_assembly(pi=true, pi_screws=true, plate=true);
        }
    }
    if (frame==3){
        screen_assembly(exploded=true);
        translate([130, -170]){
            screen_assembly(exploded=false);
        }
    }
}