use <../../openflexure-microscope/openscad/libs/utilities.scad>
use <../../openflexure-microscope/openscad/libs/threads.scad>
use <../../openflexure-microscope/rendering/librender/optics.scad>

/*
These components haven't been tested for printing. They are only for renders.

Due to the slow rendering of the thread they are pre-complied in the repository
zip-files.
*/

module m12_16fl_lens(){
    color("#404040"){
        // use m12_lens_body(tube_l=11.8); to generate this STL
        import("M12_16FL_lens_body.stl");
    }
    translate_z(12){
        lens(d=8, f=16);
    }
    translate_z(-.25){
        lens(d=10, f=99, cut=.5);
    }
}

module m12_mount_l8(){
    color("#404040"){
        // use m12_lens_holder(tube_l=8); to generate this STL
        import("M12_mount_l8.stl");

    }
}


module m12_lens_body(tube_l){
    $fn=60;
    cap_height = 4.4;
    cap_width = 14;
    difference(){
        union(){
            cylinder(h=tube_l, r=5.8);
            hull(){

                chamfer = 0.5;
                translate_z(tube_l+chamfer){
                    cylinder(h=cap_height-2*chamfer, d=cap_width);
                }
                translate_z(tube_l){
                    cylinder(h=cap_height, d=cap_width-2*chamfer);
                }
            }
            outer_thread(radius=6-.25,
                         thread_height=0.3,
                         thread_base_width=0.45,
                         thread_top_width=0.05,
                         thread_length=tube_l,
                         pitch=0.5,
                         extra=-0.5,
                         overlap=0,
                         number_divisions=60);
        }
        cylinder(h=3*tube_l, d=7, center=true);
        translate_z(tube_l){
            cylinder(h=cap_height, d=7.5);
        }
        translate_z(tube_l+cap_height-1){
            cylinder(h=1.01, d1=7.5, d2=9.2);
        }
    }
}

//Use this to generate the holder
//m12_lens_holder(tube_l=8);

module m12_lens_holder(tube_l){
    base = m12_base_size();
    difference(){
        union(){
            difference(){
                m12_lens_holder_outer(tube_l=tube_l);
                cylinder(r=6, h=2*(tube_l+base.z+1), $fn=64, center=true);
            }
            translate_z(base.z-2){
                inner_thread(radius=6-.25,
                             thread_height=0.3,
                             thread_base_width=0.45,
                             thread_top_width=0.05,
                             thread_length=tube_l+3,
                             pitch=0.5,
                             extra=-0.5,
                             overlap=0,
                             number_divisions=60);
            }
        }
        //Trim thread
        m12_lens_base_inner();
        translate_z(base.z+tube_l){
            cylinder(r=10, h=2);
        }
    }
}

function m12_base_size() = [13.8, 13.8, 3.5];

module m12_lens_holder_outer(tube_l){

    base = m12_base_size();

    translate_z(base.z/2){
        cube(base, center=true);
    }
    hull(){
        reflect_x(){
            translate_x(9){
                cylinder(d=4, h=base.z, $fn=16);
            }
        }
    }
    translate_z(base.z){
        for (n= [0:1:15]){
            rotate((n+0.5)*360/16){
                translate_x(6){
                    cylinder(h=tube_l+tiny(), d=2.6, $fn=12);
                }
            }
        }
        cylinder(r=6.9, h=tube_l+tiny(), $fn=64);
    }
}

module m12_lens_base_inner(){
    base = m12_base_size();
    inner_size = base - [1.5, 1.5, 0];

    translate_z(inner_size.z/2-0.75){
        cube(inner_size, center=true);
    }

    reflect_x(){
        translate_x(9){
            cylinder(d=2, h=3*base.z, $fn=16, center=true);
        }
    }

}