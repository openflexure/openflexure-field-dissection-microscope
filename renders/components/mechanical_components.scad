
module bearing_608(center=false){
    $fn=60;
    z_tr = center ? 0 : -3.5;
    translate([0, 0, z_tr]){
        color("Silver"){
            difference(){
                hull(){
                    cylinder(h=7-1, d=22, center=true);
                    cylinder(h=7, d=22-1, center=true);
                }
                cylinder(h=8, d=19, center=true);
            }
            difference(){
                cylinder(h=7, d=12, center=true);
                cylinder(h=8, d=8, center=true);
            }
        }
        color("#404040"){
            difference(){
                cylinder(h=6.8, d=19, center=true);
                cylinder(h=8, d=12, center=true);
            }
        }
    }
}

module m6_washer(){
    color("Silver"){
        import("m6_washer.stl");
    }
}

module m6_nut(center=false){
    nut_tr = center ? [0, 0, -2.5] : [0, 0, 0];
    translate(nut_tr){
        color("Silver"){
            import("m6_nut.stl");
        }
    }
}

module m6_cap_x25(){
    color("Silver"){
        import("m6_cap_x25.stl");
    }
}

module m3_washer(){
    color("Silver"){
        import("m3_washer.stl");
    }
}

module m3_nut(center=false){
    nut_tr = center ? [0, 0, -1.15] : [0, 0, 0];
    translate(nut_tr){
        color("Silver"){
            import("m3_nut.stl");
        }
    }
}

module m3_cap_x16(){
    color("Silver"){
        import("m3_cap_x16.stl");
    }
}

module m3_hex_x25(){
    color("Silver"){
        import("m3_hex_x25.stl");
    }
}


module no2_x6_5_selftap(flip=false){
    color("Silver"){
        vec = flip ? [180, 0, 0] : [0, 0, 0];
        rotate(vec){
            import("no2_x6_5_selftap.stl");
        }
    }
}

