use <../openflexure-microscope/openscad/libs/utilities.scad>
use <../openflexure-microscope/rendering/prepare_picamera.scad>
use <../openflexure-microscope/rendering/librender/render_utils.scad>
use <./focus_assembly.scad>

FRAME=5;
render_camera_assembly_steps(FRAME);

module render_camera_assembly_steps(frame){
    if (frame == 1){
        camera_dovetail_assembly(nut=true, thumbscrew=false, explode="nut");
        translate_z(35){
            camera_dovetail_assembly(nut=true, thumbscrew=true, explode="thumbscrew");
        }
        translate_z(70){
            camera_dovetail_assembly(nut=true, thumbscrew=true);
        }
    }
    if (frame == 2){
        render_picamera_frame(picam_frame_parameters(1));
        translate_x(35){
            render_picamera_frame(picam_frame_parameters(2));
        }
        translate_x(70){
            render_picamera_frame(picam_frame_parameters(3));
        }
    }
    if (frame == 3){
        m12_assembly(mount=true, lens=false, mounting_screws=false, explode="mount");
        translate([25,25]){
            m12_assembly(mount=true, lens=true, mounting_screws=false, explode="lens");
        }
        translate([50,50]){
            m12_assembly(mount=true, lens=true, mounting_screws=false);
        }
    }
    if (frame == 4){
        camera_mount_assembly(camera_and_lens=true, illumination=false, explode="camera_and_lens");
        translate([-20, -70]){
                    camera_mount_assembly(camera_and_lens=true, illumination=true, explode="illumination");

        }
        translate([-40, -140]){
                    camera_mount_assembly(camera_and_lens=true, illumination=true);
        }
    }
    if (frame == 5){
        ribbon_start = create_placement_dict([0, -13, -2], [0, 0, -90]);
        ribbon_end_straight = create_placement_dict([0, -213, -2], [0, 0, -90]);
        straight_positions = [ribbon_start, ribbon_end_straight];

        ribbon_bend2 = create_placement_dict([0, -14, 0], [0, 0, -90]);
        ribbon_bend3 = create_placement_dict([0, -14, 200], [-90, 0, 0], [0, 0, -90]);
        bent_positions = [ribbon_start, ribbon_bend2, ribbon_bend3];

        camera_mount_assembly_with_ribbon_cable(explode_ribbon=true, ribbon_positions=straight_positions);
        translate([-60, -20]){
            camera_mount_assembly_with_ribbon_cable(ribbon_positions=straight_positions);
        }
        translate([-120, -40]){
            camera_mount_assembly_with_ribbon_cable(ribbon_positions=bent_positions);
        }

    }

}