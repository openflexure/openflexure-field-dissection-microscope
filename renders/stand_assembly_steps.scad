use <../openflexure-microscope/openscad/libs/utilities.scad>
use <./stand_assembly.scad>

FRAME=6;
render_stand_assembly_steps(FRAME);

module render_stand_assembly_steps(frame){
    if (frame == 1){
        riser_with_nuts(base_nut="explode",
                        top_nuts="hide",
                        backplane_nuts="hide");
        translate_x(90){
            riser_with_nuts(base_nut="tighten",
                        top_nuts="hide",
                        backplane_nuts="hide");
        }
        translate_x(180){
            riser_with_nuts(base_nut="in_place",
                        top_nuts="hide",
                        backplane_nuts="hide");
        }
    }
    if (frame == 2){
        riser_with_nuts(base_nut="in_place",
                        top_nuts="explode",
                        backplane_nuts="hide");
        translate([90,-30]){
            riser_with_nuts(base_nut="in_place",
                        top_nuts="tighten",
                        backplane_nuts="hide");
        }
        translate([180,-60]){
            riser_with_nuts(base_nut="in_place",
                        top_nuts="in_place",
                        backplane_nuts="hide");
        }
    }
    if (frame == 3){
        riser_with_nuts(base_nut="in_place",
                        top_nuts="in_place",
                        backplane_nuts="explode");
        translate([-110,0,0]){
            riser_with_nuts(base_nut="in_place",
                        top_nuts="in_place",
                        backplane_nuts="tighten");
        }
        translate([-220,0,0]){
            riser_with_nuts(base_nut="in_place",
                        top_nuts="in_place",
                        backplane_nuts="in_place");
        }
    }
    if (frame == 4){
        full_render(base=true,
                    electronics=false,
                    optics=false,
                    explode="base");
        translate([-180,0,0]){
            full_render(base=true,
                        electronics=false,
                        optics=false);
        }
    }
    if (frame == 5){
        full_render(base=true,
                    electronics=true,
                    optics=false,
                    explode="electronics");
        translate([200,-20,0]){
            full_render(base=true,
                        electronics=true,
                        optics=false);
        }
    }
    if (frame == 6){
        full_render(base=true,
                    electronics=true,
                    optics=true,
                    explode="optics");
    }
}