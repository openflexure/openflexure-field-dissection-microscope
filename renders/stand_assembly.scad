use <../openflexure-microscope/openscad/libs/utilities.scad>
use <../openflexure-microscope/openscad/libs/libdict.scad>
use <../openflexure-microscope/openscad/libs/lib_microscope_stand.scad>
use <../openflexure-microscope/rendering/librender/render_utils.scad>
use <../openflexure-microscope/rendering/librender/electronics.scad>
use <../openscad/libs/stand.scad>
use <../openscad/libs/parameters.scad>
use <../openscad/libs/interfaces.scad>
use <../openscad/libs/electronics_mounting.scad>
use <./focus_assembly.scad>
use <components/mechanical_components.scad>

full_render();

module full_render(base=true, electronics=true, optics=true, explode=undef){

    riser_with_nuts();

    if (base){
        exploded = explode=="base";
        shift = exploded ? -20 : 0;
        translate_z(shift){
            coloured_render("Grey"){
                base_ring();
            }
        }

        translate(riser_screw_pos()){
            rotate_x(180){
                translate_z(2.5-5*shift){
                    m6_cap_x25();
                }
                translate_z(1.5-3*shift){
                    m6_washer();
                }
                construction_line([0,0,5*shift],[0,0,-5*shift], width=0.4);
            }
        }
    }

    if (electronics){
        exploded = explode=="electronics";

        electronics_assembly(exploded);

    }

    if (optics){
        exploded = explode=="optics";
        shift = exploded ? 4 : 0;

        focus_z_pos = focus_assembly_z()-key_lookup("overall_height", focus_dt_params())/2;


        translate([0, shift, focus_z_pos]){
            focus_assembly();
        }

        translate([0, backplane_y_pos(), focus_assembly_z()]){
            for_each_backplane_screw(){
                rotate_x(-90){
                    translate_z(8+2*shift){
                        m6_washer();
                    }
                    translate_z(9+10*shift){
                        m6_cap_x25();
                    }
                    construction_line([0,0,-shift], [0,0,8*shift], width=0.2);
                }
            }
        }

    }
}

module riser_with_nuts(base_nut="in_place",
                       top_nuts="in_place",
                       backplane_nuts="in_place"){

    coloured_render("DodgerBlue"){
        riser();
    }

    if (base_nut != "hide"){
        nutshift = (base_nut == "tighten") ?
                        [0, 0, -5] :
                        (base_nut == "explode") ?
                            [0, 60, -5] :
                            [0, 0, 0];
        translate(riser_screw_pos()){
            rotate_x(180){
                translate(nutshift+[0, 0, -15]){
                    rotate(30){
                        m6_nut();
                    }
                }
                if (base_nut == "explode"){
                    translate_z(-13){
                        construction_line(nutshift, [0,0,-5], width=0.3);
                    }
                }

                if (base_nut == "tighten" || base_nut == "explode"){
                    screw_shift = base_nut == "explode"? 40 : 0;
                    translate_z(-3.5+screw_shift){
                        m6_cap_x25();
                    }
                    translate_z(-5+screw_shift/4){
                        m6_washer();
                    }
                    if (base_nut == "tighten"){
                        translate_z(-1){
                            turn_clockwise(10, line_w=0.3);
                        }
                    }
                    if (base_nut == "explode"){
                        construction_line([0,0,20], [0,0,-screw_shift], width=0.3);
                    }
                }
            }
        }
    }

    if (top_nuts != "hide"){
        nutshift = (top_nuts == "tighten") ?
                        [0, 0, -5] :
                        (top_nuts == "explode") ?
                            [0, -60, -5] :
                            [0, 0, 0];
        for (hole = riser_top_holes()){
            translate(hole){
                translate([0, 0, -5]+nutshift){
                    rotate_z(30){
                        m6_nut();
                    }
                }
                if (top_nuts == "explode"){
                    translate_z(-13){
                        construction_line(nutshift+[0,0,10], [0,0,5], width=0.3);
                    }
                }
                if (top_nuts == "tighten" || top_nuts == "explode"){
                    screw_shift = top_nuts == "explode"? 40 : 0;
                    translate_z(11+screw_shift){
                        m6_cap_x25();
                    }
                    translate_z(10+screw_shift/4){
                        m6_washer();
                    }
                    if (top_nuts == "tighten"){
                        translate_z(18){
                            turn_clockwise(8, line_w=0.3);
                        }
                    }
                    if (top_nuts == "explode"){
                        construction_line([0,0,30], [0,0,-screw_shift], width=0.3);
                    }
                }
            }
        }
    }

    if (backplane_nuts != "hide"){

        translate([0, backplane_y_pos(), focus_assembly_z()]){
            for_left_backplane_screws(){
                nutshift = (backplane_nuts == "tighten") ?
                            [0, 0, -5] :
                            (backplane_nuts == "explode") ?
                                [-40, 0, -5] :
                                [0, 0, 0];
                rotate_x(-90){
                    translate([0, 0, -16]+nutshift){
                        m6_nut();
                    }
                    if (backplane_nuts == "explode"){
                        translate_z(-13){
                            construction_line(nutshift, [0,0,-5], width=0.3);
                        }
                    }
                }
            }
            for_right_backplane_screws(){
                nutshift = (backplane_nuts == "tighten") ?
                            [0, 0, -5] :
                            (backplane_nuts == "explode") ?
                                [40, 0, -5] :
                                [0, 0, 0];
                rotate_x(-90){
                    translate([0, 0, -16]+nutshift){
                        m6_nut();
                    }
                    if (backplane_nuts == "explode"){
                        translate_z(-13){
                            construction_line(nutshift, [0,0,-5], width=0.3);
                        }
                    }
                }
            }
            for_each_backplane_screw(){
                rotate_x(-90){
                    if (backplane_nuts == "tighten" || backplane_nuts == "explode"){
                        screw_shift = backplane_nuts == "explode"? 40 : 0;
                        translate_z(1+screw_shift){
                            m6_cap_x25();
                        }
                        translate_z(screw_shift/4){
                            m6_washer();
                        }
                        if (backplane_nuts == "tighten"){
                            translate_z(5){
                                turn_clockwise(8, line_w=0.3);
                            }
                        }
                        if (backplane_nuts == "explode"){
                            construction_line([0,0,30], [0,0,-screw_shift], width=0.3);
                        }
                    }
                }
            }
        }
    }
}

module electronics_assembly(exploded=false){

    shift = exploded ? 25 : 0;
    translate_z(2*shift){
        screen_assembly();
    }

    translate_z(shift){
        pi_tray_assembly();
    }

    for (hole = riser_top_holes()){
        translate(hole){
            translate_z(20+3.2*shift){
                m6_cap_x25();
            }
            translate_z(19+2.1*shift){
                m6_washer();
            }
            construction_line([0,0,-shift], [0,0,3*shift], width=0.4);
        }
    }
}


module screen_assembly(exploded=false){

    shift = exploded ? 10 : 0;
    coloured_render("DodgerBlue"){
        screen_mount();
    }
    screen_dims = standard_screen_size();
    excess = screen_bevel();
    at_screen_mounting_loc(screen_dims, excess, rel_to_mount=false){
        translate_z(14+6*shift){
            m3_cap_x16();
        }
        translate_z(13.5+4*shift){
            m3_washer();
        }
        translate_z(-shift){
            m3_nut();
        }
        if (exploded){
            construction_line([0,0,-shift],[0,0,6*shift], width=0.3);
        }
    }

    at_screen_mounting_loc(screen_dims, excess, each_screw=false, rel_to_mount=false){
        translate([165/2, 107/2, 12+shift]){
            rotate_z(180){
                display_7inch_lcd();
            }
        }
    }

}

module pi_tray_assembly(pi=true, pi_screws=true, plate=true, explode=undef){

    coloured_render("DodgerBlue"){
        pi_tray();
    }

    if (pi){
        exploded = explode=="pi";
        shift = exploded ? [-15, 30, 10] : [0,0,0];

        position_pi_tray_stl(){
            translate([3, 3, 5.5] + shift){
                rpi_4b();
            }
            if (exploded){
            construction_line([85,5,15], [85,5,15]+shift, width=0.2);
            }
        }
    }

    if (pi_screws){
        exploded = explode=="pi_screws";
        shift = exploded ? 30 : 0;

        position_pi_tray_stl(){
            for (hole = pi_hole_pos(inset_for_stand=true)){
                translate([hole.x, hole.y, 7+shift]){
                    no2_x6_5_selftap();
                    if (exploded){
                        construction_line([0,0,-2], [0,0,-shift], width=0.2);
                    }
                }
            }
        }
    }

    if (plate){
        exploded = explode=="plate";
        shift = exploded ? 15 : 0;
        coloured_render("Grey"){
            translate_z(shift){
                electronics_mounting_plate();
            }
        }
        position_pi_tray_stl(){
            base_size = electronics_drawer_base_size();
            mounts = pi_tray_mounts(base_size);
            for (mount = mounts){
                mount_hole = mount[0];
                translate([mount_hole.x, mount_hole.y, 2+shift]){
                    no2_x6_5_selftap();
                    if (exploded){
                        construction_line([0,0,-2], [0,0,-2-2*shift], width=0.2);
                    }
                }
            }
        }
    }
}