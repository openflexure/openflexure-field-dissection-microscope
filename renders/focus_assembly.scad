use <../openflexure-microscope/openscad/libs/utilities.scad>
use <../openflexure-microscope/openscad/libs/libdict.scad>
use <../openflexure-microscope/openscad/libs/cameras/camera.scad>
use <../openflexure-microscope/openscad/libs/gears.scad>
use <../openflexure-microscope/rendering/librender/render_utils.scad>
use <../openflexure-microscope/rendering/librender/electronics.scad>
use <../openflexure-microscope/rendering/librender/optics.scad>

use <../openscad/libs/parameters.scad>
use <../openscad/libs/optics_dovetails.scad>
use <../openscad/libs/optics_mounts.scad>
use <../openscad/libs/rack_and_gear.scad>
use <../openscad/libs/motion_controls.scad>
use <../openscad/main_gear.scad>

use <components/m12_components.scad>
use <components/mechanical_components.scad>

focus_assembly();

function final_illumination_cable_points() = let(
    clip_pos = camera_cable_clip_pos(focus_female_dt_params())
) [clip_pos+[0,12,1.5],
   clip_pos+[0,-3,1.5],
   clip_pos+[-10,-4,8],
   [-30.4, backplane_y_pos()+6, stand_top_z()-focus_assembly_z()],
   [-30.4, backplane_y_pos()+6, stand_top_z()-focus_assembly_z()+10]];

function final_camera_ribbon_positions() = let(
    ribbon_start = create_placement_dict([0, -13, -2], [0, 0, -90]),
    ribbon_1 = create_placement_dict([0, -14, 0], [0, 0, -90]),
    ribbon_2 = create_placement_dict([0, -14, 20], [0, 0, -90]),
    ribbon_3 = create_placement_dict(camera_cable_clip_pos(focus_female_dt_params())+[0,12,1], [0, 0, -90]),
    ribbon_4 = create_placement_dict(camera_cable_clip_pos(focus_female_dt_params())+[0,-5,1], [0, 0, -90]),
    ribbon_end = create_placement_dict([3, backplane_y_pos()+49, stand_top_z()-focus_assembly_z()-12],  [0, -90, 0], init_translation=[15, 0, 0])
) [ribbon_start,ribbon_1, ribbon_2, ribbon_3, ribbon_4, ribbon_end];


module focus_assembly(carrige_and_camera=true, cable_clip=true, explode=undef, tighten_thumbscrew=false){

    illumination_points = final_illumination_cable_points();
    positions = final_camera_ribbon_positions();

    pinion_housing_assembly();

    if (carrige_and_camera){
        exploded = explode=="carrige_and_camera";
        shift = exploded ? 70 : 0;

        translate_z(-shift){
            coloured_render("DodgerBlue"){
                optics_carriage(focus_female_dt_params(),
                                optics_male_dt_params(),
                                gearing_params());
            }
        }
        translate_z(shift){
            camera_mount_assembly_with_ribbon_cable(ribbon_positions=positions,
                                                    illumination_cable_points=illumination_points,
                                                    tighten_thumbscrew=tighten_thumbscrew);
        }
        if (exploded){
            translate(camera_cable_clip_pos(focus_female_dt_params())){
                translate_y(-8){
                    construction_line([0,0,-shift/2],[0,0,-shift],width=.2);
                }
                translate_y(20){
                    construction_line([0,0,shift/4],[0,0,-shift], width=.2);
                }
            }
        }
    }

    if (cable_clip){
        exploded = explode=="cable_clip";
        shift = exploded ? [0,0,20] : [0,0,0];
        translate(camera_cable_clip_pos(focus_female_dt_params())+shift){
            coloured_render("Grey"){
                camera_cable_clip();
            }

            for(x_tr = [-0.5, 0.5]*camera_cable_clip_hole_sep()){
                translate([x_tr, 0, 3]+shift){
                    no2_x6_5_selftap();
                    construction_line([0,0,0],-2.5*shift,width=0.2);
                }
            }
        }
    }

}


module pinion_housing_assembly(exploded=false){
    shift = exploded ? 20 : 0;
    coloured_render("DodgerBlue"){
        translate_y(4*shift){
            main_gear(gearing_params());
        }
        translate_x(-4*shift){
            main_gear_placement(gearing_params()){
                focus_knob(gearing_params());
            }
        }
        translate_x(focus_shaft_separation()/2+2*shift){
            main_gear_placement(gearing_params()){
                rotate_y(180){
                    focus_knob(gearing_params(), shaft=false);
                }
            }
        }
    }

    translate_x(focus_shaft_separation()/2+2+4*shift){
        main_gear_placement(gearing_params()){
            no2_x6_5_selftap();
        }
    }


    dt_width = key_lookup("overall_width", focus_dt_params());
    reflect_x(){
        translate_x(dt_width/2+1+shift){
            main_gear_placement(gearing_params()){
                bearing_608(center=true);
            }
        }
    }

    coloured_render("Grey"){
        pinion_housing(focus_dt_params(), gearing_params());
    }

    if (exploded){
        main_gear_placement(gearing_params()){
            construction_line([0,0,-4*shift], [0,0,5*shift], width=0.2);
        }
    }
    if (exploded){
        main_gear_placement(gearing_params()){
            construction_line([0,0,0], [0,4*shift,0], width=0.2);
        }
    }
}

module camera_mount_assembly_with_ribbon_cable(ribbon_positions,
                                               explode_ribbon=false,
                                               illumination_cable_points=[[15, -4, 148]],
                                               tighten_thumbscrew=false){

    camera_mount_assembly(illumination_cable_points=illumination_cable_points,
                          camera_connector_open=explode_ribbon,
                          tighten_thumbscrew=tighten_thumbscrew);
    translate_y(explode_ribbon ? -20 : 0){
        picamera_cable(ribbon_positions);
    }
}


module led_assembly(){
    rotate_x(180){
        led();
    }
    coloured_render("DimGrey"){
        cylinder(d=4, h=40, $fn=16);
    }

}

module led_wires(points){

    red_points = [for (i = points) i-[2.54/2,0,0]];
    blk_points = [for (i = points) i+[2.54/2,0,0]];
    coloured_render("red"){
        wire(d=1, points=red_points);
    }
    coloured_render("DimGrey"){
        wire(d=1, points=blk_points);
        translate(points[len(points)-1]-[0,0,9]){
            dupont_connector_housing(2, center=true);
        }
    }
}


function condenser_clamp_axis_pos(depth) = let(
    tr = [0, -40.8, 9.8],
    r1 = [0,0,120],
    init_tr = [0, -depth, 0]
) create_placement_dict(tr, r1, init_translation=init_tr);


module camera_mount_assembly(camera_and_lens=true,
                             illumination=true,
                             explode=undef,
                             illumination_cable_points=[[15, -4, 148]],
                             camera_connector_open=false,
                             tighten_thumbscrew=false){

    camera_dovetail_assembly(tighten_thumbscrew=tighten_thumbscrew);

    if (camera_and_lens){
        exploded = explode=="camera_and_lens";
        extra_z = exploded ? -20 : 0;
        translate_z(-3.5+extra_z){
            rotate(90){
                rotate([180, 0, 0]){
                    picamera2(lens=false, connector_open=camera_connector_open);
                }
            }
        }

        translate_z(-3.5+2*extra_z){
            rotate_z(-135){
                rotate_x(180){
                    if (exploded){
                        m12_assembly(explode="mounting_screws");
                    }else{
                        m12_assembly();
                    }
                }
            }
        }
    }

    if (illumination){
        exploded = explode=="illumination";
        shift = exploded ? 15 : 0;

        translate_x(shift){
            translate([15, -4, -22]){
                led_assembly();
            }
            all_illumination_cable_points = concat([[15, -4, 18]], illumination_cable_points);
            led_wires(all_illumination_cable_points);
        }

        coloured_render("DodgerBlue"){
            illumination_clip_placement(){
                translate_z(2*shift){
                    illumination_clip();
                }
            }
        }


        illumination_clip_placement(){
            hole_spacing = illumination_clip_hole_spacing();
            for(x_tr = [-0.5, 0.5]*hole_spacing){
                translate([x_tr, 0, 2+3*shift]){
                    no2_x6_5_selftap();
                    if (exploded){
                        construction_line([0,0,0], [0,0,-2-3*shift]);
                    }
                }
            }
        }
    }
}

module camera_dovetail_assembly(nut=true, thumbscrew=true, explode=undef, tighten_thumbscrew=false){
    coloured_render("Grey"){
        simple_camera_mount(optics_dt_params());
    }

    if (nut){
        exploded = explode=="nut";
        shift = exploded ? [-10,0,5] : [0,0,0];
        place_part(condenser_clamp_axis_pos(4)){
            translate(shift){
                rotate_x(90){
                    rotate_z(30){
                        m3_nut();
                    }
                }
            }
            if (exploded){
                translate([2,-1,-1]){
                    construction_line([0,0,0], shift);
                }
            }
        }
    }

    if (thumbscrew){
        exploded = explode=="thumbscrew";
        shift = exploded ? 10 : 0;
        place_part(condenser_clamp_axis_pos(25+5*shift)){
            rotate_x(90){
                m3_hex_x25();
                if (exploded){
                    construction_line([0,0,0], [0,0,-6*shift]);
                }
            }
        }
        place_part(condenser_clamp_axis_pos(14.3+shift)){
            rotate_x(90){
                coloured_render("DodgerBlue"){
                    illumination_thumbscrew();
                }
                if (tighten_thumbscrew){
                    translate_z(14){
                        turn_clockwise(8, line_w=0.2);
                    }
                }
            }
        }
    }
}

module m12_assembly(mount=true, lens=true, mounting_screws=true, explode=undef){
    optics_config = m12_optics_config();
    camera_mount_height = camera_mount_height(optics_config);


    coloured_render("Grey"){
        m12_adapter();
    }

    if (mount){
        exploded = explode=="mount";
        extra_z = exploded ? 20 : 0;
        translate_z(camera_mount_height+extra_z){
            rotate_z(90-m12_mount_angle()){
                m12_mount_l8();
            }
        }

        for (y_tr = [-9, 9]){
            rotate_z(-m12_mount_angle()){
                translate([0, y_tr, 2-extra_z]){
                    no2_x6_5_selftap(flip=true);
                    if (exploded){
                        construction_line([0,0,0],[0,0,2.2*extra_z]);
                    }
                }
            }
        }

    }

    if (lens){
        exploded = explode=="lens";
        extra_z = exploded ? 5 : 0;
        translate_z(camera_mount_height+6+extra_z){
            m12_16fl_lens();
            if (exploded){
                translate_z(16){
                    rotate_z(-20){
                        turn_clockwise(8);
                    }
                }
            }
        }
    }

    if (mounting_screws){
        exploded = explode=="mounting_screws";
        extra_z = exploded ? 15 : 0;
        rotate(135){
            holes = picamera2_holes();
            for (hole_pos = holes){
                screw_z=1;
                translate(hole_pos + [0, 0, screw_z+extra_z]){
                    no2_x6_5_selftap();
                    construction_line([0,0,0],[0,0,-4*extra_z]);
                }
            }
        }
    }
}




