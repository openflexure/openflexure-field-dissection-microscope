use <../openflexure-microscope/openscad/libs/utilities.scad>
use <./focus_assembly.scad>

FRAME=3;
render_focus_assembly_steps(FRAME);

module render_focus_assembly_steps(frame){
    if (frame == 1){
        pinion_housing_assembly(exploded=true);
        translate_z(-100){
            pinion_housing_assembly();
        }
    }
    if (frame == 2){
        focus_assembly(carrige_and_camera=true,
                       cable_clip=false,
                       explode="carrige_and_camera");
        translate([-90,90]){
            focus_assembly(carrige_and_camera=true,
                           cable_clip=false,
                           tighten_thumbscrew=true);
        }
    }
    if (frame == 3){
        focus_assembly(carrige_and_camera=true,
                       cable_clip=true,
                       explode="cable_clip");
        translate([-90,90]){
            focus_assembly(carrige_and_camera=true,
                           cable_clip=true);
        }
    }
}