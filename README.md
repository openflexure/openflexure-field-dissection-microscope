# OpenFlexure Field Dissection Microscope

The OpenFlexure Field Dissection Microscope is a microscope for looking at objects that are a few mm across, such as bees. The microscope is in an early development stage, but eventually the microscope should be robust enough for field work.

The microscope sits in the [OpenFlexure](https://openflexure.org) family of microscopes. This field dissection microscope reuses some features from the [OpenFlexure Microscope](https://openflexure.org/projects/microscope/), including the a tried and tested methods for mounting optics on locking dovetails. It also reuses much of the same CAD codebase.

Development of this first prototype was supported by crowd-funding on [Experiment.com](https://experiment.com/projects/creating-a-field-dissection-microscope-that-can-be-built-in-the-field) and by an Experiment Foundation Grant.

Some examples of the microscopes performance can be seen on [Experiment.com](https://experiment.com/u/p16uMQ)

## Build a microscope

To build your microscope follow the [assembly instructions](https://openflexure.gitlab.io/openflexure-field-dissection-microscope). The assembly instructions contain all STL files for 3D printing,

## Get Involved!
This project is open so that anyone can get involved, and you don't have to learn OpenSCAD to help (although that would be great).  Ways you can contribute include:

* [Join our forum](https://openflexure.discourse.group/)
* Share your microscope images (of both microscopes and what you've seen with them) on social media - you can mention @openflexure on Twitter.
* [Open an issue](https://gitlab.com/openflexure/openflexure-field-dissection-microscope/issues/) if you spot something that's wrong, or something that could be improved. 
* Suggest better text or images for the instructions.
* Improve the design of parts - even if you don't use OpenSCAD, STL files or descriptions of changes are helpful.
* Fork it, and make merge requests - again, documentation improvements are every bit as useful as revised OpenSCAD files.

Things in need of attention are currently described in [issues](https://gitlab.com/openflexure/openflexure-field-dissection-microscope/issues/) so have a look there if you'd like to work on something but aren't sure what.

## Developing
If you want to play with the OpenSCAD files or change the documentation, you should fork the repository.  You can edit the documentation online in GitLab, or clone the repository if you want to edit the OpenSCAD files.  You will need to clone the whole repository as the OpenSCAD files are dependent on each other.

### Development environment
We mostly use VSCode to edit the OpenSCAD files, and then use OpenSCAD with the editor hidden and the "automatic reload and compile" option ticked.  This is much nicer for a big multi-file project like the microscope than relying on OpenSCAD's built-in editor, and also works nicely with version control.

### LFS files

This repository stores images using Git LFS. This means that cloning the repository without Git LFS installed will only download placeholders for the images. Follow these instructions to [install Git LFS](https://git-lfs.github.com/).

With LFS installed Git will download the latest version of the images used in the documentation. If they are still missing try running:
```
git lfs fetch
git lfs checkout
```
