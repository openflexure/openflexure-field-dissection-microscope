# Assembling the optics

{{BOM}}

[No 2 6.5mm self tapping screws]: parts/mechanical.yml#SelfTap_PoziPan_No2x6.5_SS "{cat:mech}"
[M3 nuts]: parts/mechanical.yml#Nut_M3_SS "{cat:mech}"
[M3x25mm stainless steel hex bolt]: parts/mechanical.yml#HexBolt_M3x25mm_SS "{cat:mech}"
[Raspberry Pi Camera Module v2]: parts/electronics.yml#PiCamera_2 "{cat:electronic}"
[Pi Camera lens tool]: parts/electronics.yml#PiCamera_2 "{cat:tool, note: 'This should come with the camera.'}"
[200mm Pi Camera ribbon cable]: parts/electronics.yml#PiCamera_RibbonCable_200mm "{cat:electronic, note: 'This is longer than the standard ribbon cable the Pi Camera is sold with!'}"
[M12 lens holder (8mm long)]: parts/optics/M12_lens_holder.md "{cat:optical}"
[16mm focal length M12 lens]: parts/optics/M12_lens.md "{cat:optical}"
[#1 pozidrive screwdriver]: parts/tools/pozidrive_1_screwdriver.md "{cat:tool}"

## Attach dovetail locking thumbscrew {pagestep}

![](../builds/renders/camera_dovetail_assembly.png)


* Slide an [M3 nut][M3 nuts]{qty:1} into the slot in the dovetail on the [camera mount][Camera Mount](fromstep){cat:PrintedPart, qty:1}
* Also insert a [M3 hex bolt][M3x25mm stainless steel hex bolt]{qty:1} into the [Optics Thumbscrew](fromstep){cat:PrintedPart, qty:1}
* Screw the thumbscrew into the camera mount loosely

## Build the lens assembly {pagestep}

![](../builds/renders/m12_adapter_assembly.png)


* Take the printed [M12 adapter][M12 Adapter](fromstep){cat:PrintedPart, qty:1} and an [M12 lens holder][M12 lens holder (8mm long)]{qty:1} with an 8mm long threaded section.
* Position the lens holder over the two holes on the front of the adapter.
* Use a [#1 pozidrive screwdriver]{qty:1} to secure the holder in place with two [self-tapping screws][No 2 6.5mm self tapping screws]{qty:2}.
* Start screwing a [16mm focal length M12 lens]{qty:1} into the holder, but leave most of the thread still visible.


## Remove Pi Camera Lens {pagestep}

![](../builds/renders/picamera_lens_removal.png)

>! **Caution!**
>!
>! The camera board is static sensitive.

* Before touching the Pi Camera touch a metal earthed object. If you own one, consider wearing and anti-static strap.
* Take the [Pi Camera][Raspberry Pi Camera Module v2]{Qty:1} out of the package. Make sure to **hold it only by the sides of the board**.
* Take the protective film off the lens.
* Take the [Pi Camera lens tool]{qty: 1} and place it over the lens
* Slowly unscrew the lens (About 4 full turns of the tool)
* Carefully lift off the lens. We do not use the lens in this version of the microscope.


## Attach Camera and LED {pagestep}

![](../builds/renders/camera_mount_assembly.png)

* Position the pi camera between the m12 lens assembly and the camera mount. So that the mounting holes are over the standoffs and cable clip faces the dovetail.
* Use four [self-tapping screws][No 2 6.5mm self tapping screws]{qty:4} to secure the camera and lens assembly to the mount.
* Place the [illumination clip][Illumination clip](fromstep){cat:PrintedPart, qty:1} around the [LED assembly](fromstep){cat:subassembly, qty:1} and align with the two holes on the side of the camera mount.
* Use two [self-tapping screws][No 2 6.5mm self tapping screws]{qty:2} to attach the clip to the mount.
* Before fully tightening adjust the LED to be about level with the camera lens.



## Attach Camera cable {pagestep}

![](../builds/renders/camera_ribbon_assembly.png)

* Open the ribbon cable connector on the camera.
* Inset the [ribbon cable] into the camera with the contacts facing outwards away from the camera mount.
* Feed the excess ribbon cable through the camera mount slot.

[camera and optics assembly]{output, qty:1, hidden}
