## 7 inch LCD screen

The design uses a 7 inch LCD screen with an HDMI port. The screen is a touch screen and uses MicroUSB ports for data communication and power. Many of these generic screens exist. Any of them should work but the screen mount may need adjusting.

This design uses a screen with a hole spacing of 157 mm x 116 mm.

