 ---
PartData:
    Specs:
        Mounting Type: S-Mount (M12)
        Effective focal length: 16 mm
        Back focal length: 7.1 mm
        Construction: 6G+IR
        Working Wavelength: 400-700 nm
    Suppliers:
        Arducam:
            PartNo: M2016ZH01
            Link: https://www.arducam.com/product/m2016zh01-2/
---

The M12 lens used for testing the microscope is an Arducam M2016ZH01 with an effective focal length of 16mm. Other M12 lenses can be used for different optical performance.

Arducam also do a [set of M12 lenses](https://www.amazon.com/Arducam-Telephoto-Fisheye-Cleaning-Optical/dp/B096V2NP2T?ref_=ast_sto_dp) that include this lens and the lens holder that is needed.