 ---
PartData:
    Specs:
        Mounting Type: S-Mount (M12)
        Thread Length: 8mm
        Hole spacing: 18 mm
        Total height: 11.5 mm
---

If you can't find the exact same lens holder. A taller one would be prefereable.

Arducam also do a [set of M12 lenses](https://www.amazon.com/Arducam-Telephoto-Fisheye-Cleaning-Optical/dp/B096V2NP2T?ref_=ast_sto_dp) that include this lens holder and the lens that is needed.