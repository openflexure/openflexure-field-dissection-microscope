# Raspberry Pi Camera Lens Removal Tool

The [Raspberry Pi Camera Module v2](../electronics.yml#PiCamera_2) should be supplied with a white plastic tool to unscrew the lens.  If this is missing, the OpenFlexure Microscope provides a [workaround using a printed tool](https://build.openflexure.org/openflexure-microscope/v7.0.0-beta1/workaround_lens_remover.html).