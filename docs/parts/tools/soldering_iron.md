# Soldering Iron

A soldering iron is required to make up the LED and resistor assembly. Any iron will do provided it is compatible with your chosen solder, but a relatively large chisel tip is probably best for wire-to-wire soldering.