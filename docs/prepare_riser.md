# Prepare the riser

{{BOM}}

[M6 nuts]: parts/mechanical.yml#Nut_M6_SS "{cat:mech}"

## Inserting the base nut {pagestep}
![](../builds/renders/insert_base_nut.png)

*  Take the [riser][Riser](fromstep){qty:1, cat:printedpart} and one [M6 nut][M6 nuts]{qty:1}
*  Place the nut into the rectangular hole by the base (the end with the two raised keys)
*  Put an [M6 washer][extra M6 washer](parts/mechanical.yml#CapScrew_M6x25mm_SS){qty: 1, cat:tool} onto a [M6x25 cap head screw][extra M6x25 cap screw](parts/mechanical.yml#CapScrew_M6x25mm_SS){qty: 1, cat:tool}
* Feed the screw into the round hole on the base until it engages with the nut
* Tighten the screw using a [5mm hex key](parts/tools/metric_hex_key_set.md){cat:tool, qty:1} so it pulls the nut firmly into the nut trap. This will take a fair amount of torque!
* Check the nut is now below the rectangular channel
* Remove the screw. The nut should stay in place.

## Inserting the backplane nuts {pagestep}
![](../builds/renders/insert_backplane_nuts.png)

* Place an [M6 nut][M6 nuts]{qty:4} in one of the rectangular holes on the side of the riser
* Repeat the same process as above to pull the nut home using the [M6 washer][extra M6 washer](parts/mechanical.yml#CapScrew_M6x25mm_SS){qty: 1, cat:tool} and [M6 screw][extra M6x25 cap screw] in the round hole in the front of the riser.
* Remove the screw. The nut should stay in place.
* Repeat for the other holes

## Inserting the top nuts {pagestep}
![](../builds/renders/insert_top_nuts.png)

* Repeat the same process again to seat [M6 nuts]{qty:4} under for the holes in the top of the riser. Note that two nuts go into each rectangular hole.

[prepared riser]{output, qty:1, hidden}
