# OpenFlexure Field Dissection Microscope

## About this project

The OpenFlexure Field Dissection Microscope is a microscope for looking at objects that are a few mm across, such as bees. The microscope is in an early development stage, but eventually the microscope should be robust enough for field work.

The microscope sits in the [OpenFlexure](https://openflexure.org) family of microscopes. This field dissection microscope reuses some features from the [OpenFlexure Microscope](https://openflexure.org/projects/microscope/), including the a tried and tested methods for mounting optics on locking dovetails. It also reuses much of the same CAD codebase.

Development of this first prototype was supported by crowd-funding on [Experiment.com](https://experiment.com/projects/creating-a-field-dissection-microscope-that-can-be-built-in-the-field) and by an Experiment Foundation Grant.

## Building a Field Dissection Microscope

![](../builds/renders/stand_assembly.png)

Before you start building the microscope you will need to check you have everything on our the [list of components]{BOM}. After this the assembly is divided into these steps

1. [.](printing.md){step}
1. [.](solder_led.md){step}
1. [.](prepare_riser.md){step}
1. [.](optics_assembly.md){step}
1. [.](focus_assembly.md){step}
1. [.](electronics_assembly.md){step}
1. [.](final_assembly.md){step}
1. [.](getting_started.md){step}

## Understanding the design

Some extra information is also provided in the following pages to aid in understand the design.

{{listpages, tag:info}}