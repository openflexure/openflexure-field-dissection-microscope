---
Tags: info
---

# Understanding the Gearing

This field microscopes needs a rack for the involute pinion to ride up when focusing. OpenFlexure uses its own fork of MCAD's gears for its gearing. This comes with its own confusions as MCAD isn't too well documented and has some quirks. 

There is no functionality for racks in MCAD. A mathematician might say that a rack is a section of a gear of infinite radius. But we can't type infinities into OpenSCAD and get a good result. So we will have to make our own gear profile.

## Understanding MCAD Circular pitch

Circular Pitch should be the distance between the teeth measured along the pitch circle. Where the pitch circle is the effective radius of a gear, i.e. meshed gears should have their pitch circles touching.

As OpenSCAD works in mm this should be in mm. We know that the two gears in the OpenFlexure Microscope sit 20 mm apart, and the large gear has twice as many teeth (24 vs 12). So it's pitch circle radius should be 2/3 of 20mm.

    Radius on pitch circle = 20*2/3 = 13.333 mm
    Circumference of pitch circle = 83.7758 mm
    Number of teeth = 24
    Circular pitch = 83.7758 mm/24 = 3.49 mm

Quickly measuring a printed gear this looks correct.

However, in the OpenFlexure code the gear pitch is set as 200. Begging the question 200 what!? Digging into MCAD deeper they have included the conversion from radians to degrees (as OpenSCAD uses degrees for its trigonometry). If we divide 200 by 180 then multiply by π we get 3.49 mm.

## Pressure angle

As involute gears rotate they the [angle the force is at applied remains constant](https://en.wikipedia.org/wiki/Involute_gear). This pressure angle sets the shape of the teeth. It is specified with MCAD, so we don't need to calculate it. Interestingly standard gears normally have a 20degree pressure angle. MCAD uses 28 degrees as standard. This adds more backlash and axial forces, but at the benefit of chunkier teeth. This is pretty important for the small pinion in the OpenFlexure Microscope. Our gear is much larger so we use the more standard 20 degrees.

## Double Helix Gearing

We use double helix gearing as it forces axial alignment. Usually the downside of double helix gears are higher friction. However, in our case we want the microscope to stay in place after each move, so the friction should be beneficial.

For the helical gearing MCAD provides a parameter called twist. This is the rotation over the extrusion of the tooth. This means that for gears to mesh the twist needs to be set differently depending on the number of teeth. We set a `skew` value instead, which is normalised by the gear pitch. So a skew of 1 corresponds to the top of one tooth being above the bottom of the next tooth.

Double helical gears are created by mirroring two helical gears.

## Designing the rack

The each tooth on the rack is trapezoidal in shape. We use the OpenSCAD polygon feature to create a tooth with the required skew.
