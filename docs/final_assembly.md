# Final Assembly

{{BOM}}

[M6 washers]: parts/mechanical.yml#CapScrew_M6x25mm_SS "{cat:mech}"
[M6x25 cap head screws]: parts/mechanical.yml#CapScrew_M6x25mm_SS "{cat:mech}"

## Attach the base ring to the riser {pagestep}

![](../builds/renders/attach_base.png)

* Place the [Base Ring](fromstep){cat:PrintedPart, qty:1} over the [riser][prepared riser](fromstep){cat:subassembly, qty:1} so the keys of the riser engage with the slots in the base.
* Check the 4 holes in the front of the risers face towards the ring of the base.
* Put an [M6 washer][M6 washers]{qty: 1} onto a [M6x25 cap head screw][M6x25 cap head screws]{qty:1}, and place into the hole on the bottom of the base
* Tighten the screw using a [5mm hex key](parts/tools/metric_hex_key_set.md){cat:tool, qty:1}


## Attach the electronics to the riser {pagestep}

![](../builds/renders/attach_electronics.png)

* Stand the riser up on the base.
* Position the [assembled pi tray](fromstep){cat:subassembly, qty:1} on the top of the riser so the raspberry pi faces downwards
* Position [assembled screen mount](fromstep){cat:subassembly, qty:1} over the pi tray so that all holes align.
* Secure in place with four [M6x25 cap head screws]{qty:4} (each with an [M6 washer][M6 washers]{qty: 4}).


## Attach the optics to the riser {pagestep}

![](../builds/renders/attach_optics.png)

* Position the [focus assembly] in from of the 4 holes on the front of the riser
* Secure in place with four [M6x25 cap head screws]{qty:4} (each with an [M6 washer][M6 washers]{qty: 4}).


## Connect the cables to the Pi {pagestep}

![](../openflexure-microscope/docs/diagrams/illumination_to_pi_wiring.png)

* Connect The illumination cable to the raspberry pi as shown above.
* Connect the camera ribbon cable to the port labelled camera.

## Check everything looks correct {pagestep}

Your microscope should now look like this 3D render (This may take some time to download):

![](../builds/renders/stand_assembly.glb)