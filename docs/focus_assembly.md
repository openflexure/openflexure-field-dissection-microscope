# Assembling the focussing mechanism

{{BOM}}

[No 2 6.5mm self tapping screws]: parts/mechanical.yml#SelfTap_PoziPan_No2x6.5_SS "{cat:mech}"
[#1 pozidrive screwdriver]: parts/tools/pozidrive_1_screwdriver.md "{cat:tool}"

## Attaching the main gear {pagestep}

![](../builds/renders/pinion_housing_assembly.png)

* Find the [Pinion Housing](fromstep){cat:PrintedPart, qty:1} and two [608 Bearings](parts/mechanical.yml#608_Bearing){qty:2, cat:mech}
* Place the first bearing on a solid surface and position the round recess on the side of the pinion housing over the bearing.
* Firmly press down on the housing until the bearing snaps into place
* Repeat with the second bearing on the other side of the housing
* Slot the the [Main Gear](fromstep){cat:PrintedPart, qty:1} into the front of the pinion housing.
* Taking the half of the [Focus Knob](fromstep){cat:PrintedPart, qty:1} with the long shaft, slot the shaft through either bearing on the side of the housing, though the centre of the main gear, and through the other bearing.
* Take the other half of the focus knob. Fit this onto the other side of the housing
* Ensure that the triangular recess on the knob mounts onto the triangular projection on the shaft of the other half of the knob
* Place a [self-tapping screw][No 2 6.5mm self tapping screws]{qty:1} into the hole of the second knob, and screw it into the shaft  using a [#1 pozidrive screwdriver]{qty:1}.

## Engage the dovetails{pagestep}

![](../builds/renders/engage_dovetails.png)

* Take the [camera and optics assembly](fromstep){cat:subassembly, qty:1} and slide it into the [Optics Carriage](fromstep){cat:PrintedPart, qty:1}.
* Tighten the thumbscrew to lock the camera mount in place.
* Don't worry about the exact position, this can be adjusted during first use.
* Slot the carriage into the female dovetail of the pinion housing.
* Add a drop of [light oil](parts/consumables/light_oil.md){qty:2 drops, cat:consumable}  to each side of this dovetail.

## Restrain cables with clip {pagestep}

![](../builds/renders/clip_cables.png)

* Hold the ribbon cable and illunination wires in the centre of the top of the optics carriage
* Position the [camera cable clip][Camera Cable Clip](fromstep){cat:PrintedPart, qty:1} over the wires, aligning the holes with the triangular holes on the carriage.
* Secure the clip with two [self-tapping screws][No 2 6.5mm self tapping screws]{qty:2}

[focus assembly]{output, qty:1, hidden}
