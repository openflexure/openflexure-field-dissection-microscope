# Print the plastic parts

[PLA filament]: parts/consumables/pla_filament.md "{cat:material}"
[RepRap-style printer]: parts/tools/rep-rap.md
[utility knife]: parts/tools/utility-knife.md

{{BOM}}

## Printing {pagestep}

Using an [FDM printer][RepRap-style printer]{qty:1,cat:tool} you will 3D print the components from [PLA filament]{Qty: 500g, note:"Of any colour you want. Two contrasting colours may look best."}. You need to print the following parts:

| Part name | STL File |
| -- | -- |
| [Base Ring]{output,qty:1} | [base_ring.stl](../builds/base_ring.stl){previewpage} |
| [Riser]{output,qty:1} | [riser.stl](../builds/riser.stl){previewpage} |
| [Main gear]{output,qty:1} | [main_gear.stl](../builds/main_gear.stl){previewpage} |
| [Optics Carriage]{output,qty:1} | [optics_carriage.stl](../builds/optics_carriage.stl){previewpage} |
| [Pinion Housing]{output,qty:1} | [pinion_housing.stl](../builds/pinion_housing.stl){previewpage} |
| [Camera Mount]{output,qty:1} | [simple_camera_mount.stl](../builds/simple_camera_mount.stl){previewpage} |
| [Camera Cable Clip]{output,qty:1} | [camera_cable_clip.stl](../builds/camera_cable_clip.stl){previewpage} |
| [M12 Adapter]{output,qty:1} | [m12_adapter.stl](../builds/m12_adapter.stl){previewpage} |
| [Illumination clip]{output,qty:1} | [illumination_clip.stl](../builds/illumination_clip.stl){previewpage} |
| [Focus Knob]{output,qty:1} | [focus_knob.stl](../builds/focus_knob.stl){previewpage} |
| [Optics Thumbscrew]{output,qty:1} | [optics_thumbscrew.stl](../builds/optics_thumbscrew.stl){previewpage} |
| [Electronics Mounting Plate]{output,qty:1} | [electronics_mounting_plate.stl](../builds/electronics_mounting_plate.stl){previewpage} |
| [Pi Tray]{output,qty:1} | [pi_tray.stl](../builds/pi_tray.stl){previewpage} |
| [Screen Mount]{output,qty:1} | [screen_mount.stl](../builds/screen_mount.stl){previewpage} |

**You can [download all of the STLs as a single zipfile](all-stls.zip){zip, pattern:"*.stl"}**

## Clean-up of printed parts {pagestep}

>!! **Be careful when removing brim**
>!!
>!! To avoid injury first remove the bulk of the brim without a knife. Remove the remaining brim with a peeling action as described below.

Carefully remove the printing brim from all parts (except the main body).

To remove brim:

1. Use [precision wire cutters](parts/tools/precision-wire-cutters.md){qty:1 pair, cat:tool} to remove most of the brim from the part.
2. Clean up remaining brim with a [utility knife]{qty: 1, cat: tool, note: "Not a scalpel!"}:
    * Hold the knife in your dominant hand with 4 fingers curled around the handle, leaving thumb free.
    * Hold the part in your other hand, as far away from the surface to be cut as possible.
    * Support the part with the thumb of your dominant hand.
    * Place blade on surface to be cut, and carefully close your dominant hand moving the blade, under control, towards your thumb.

![](../openflexure-microscope/docs/diagrams/BrimRemoval.png)