# Mounting the electronics

{{BOM}}

[Raspberry Pi]: parts/electronics.yml#RaspberryPi "{cat:electronic}"
[7 inch LCD screen]: parts/electronics/7inch_screen.md "{cat:electronic}"
[No 2 6.5mm self tapping screws]: parts/mechanical.yml#SelfTap_PoziPan_No2x6.5_SS "{cat:mech}"
[M3 nuts]: parts/mechanical.yml#Nut_M3_SS "{cat:mech}"
[M3 washers]: parts/mechanical.yml#Washer_M6_SS "{cat:mech}"
[M3x16mm cap screws]: parts/mechanical.yml#CapScrew_M3x16mm_SS "{cat:mech}"
[#1 pozidrive screwdriver]: parts/tools/pozidrive_1_screwdriver.md "{cat:tool}"
[2.5mm hex key]: parts/tools/metric_hex_key_set.md "{cat:tool}"

## Mount the Raspberry pi {pagestep}
![](../builds/renders/pi_tray_assembly.png)

* Slide the [Raspberry Pi]{qty:1} into the [Pi Tray](fromstep){cat:PrintedPart, qty:1} until it clicks into place.
* Secure with four [self-tapping screws][No 2 6.5mm self tapping screws]{qty:4} using a [#1 pozidrive screwdriver]{qty:1}

## Mount the pi tray {pagestep}
![](../builds/renders/pi_tray_plate_assembly.png)

* Position the Pi Tray over the [Electronics Mounting Plate](fromstep){cat:PrintedPart, qty:1} so that all four holes align.
* Make sure side of the Raspberry Pi with the GPIO pins face the 4 larger holes in the mounting plate. (If they don't, flip the mounting plate over)
* Secure with four [self-tapping screws][No 2 6.5mm self tapping screws]{qty:4}

## Mount the screen {pagestep}
![](../builds/renders/screen_assembly.png)

* Position the [7 inch LCD screen]{qty:1} on the [Screen Mount](fromstep){cat:PrintedPart, qty:1} so the holes align with the stand-offs.
* Check the HDMI port is in the top right-hand corner
* Push 4 [M3x16mm cap screws]{qty:4} (each with a [M3 washer][M3 washers]{qty:4}) through the holes to pin the screen in place.
* Carefully place the [M3 nuts]{qty:4} on the back of each screw.
* Tighten each screw with a [2.5mm hex key]{qty:1}, checking that each nut ends up secure in its nut trap.


[assembled pi tray]{output, qty:1, hidden}

[assembled screen mount]{output, qty:1, hidden}
