# Getting started with your microscope

{{BOM}}

[USB-C power cable]: parts/electronics.yml#RaspberryPi_PowerSupply
[Mini HDMI to HDMI cable]: parts/electronics.yml#MiniHDMI_HDMI_Cable
[USB to micro USB cable]: parts/electronics.yml#USB_MicroUSB_Cable
[micro SD card]: parts/electronics.yml#MicroSD_Card
[wireless keyboard with trackpad]: parts/electronics.yml#WirelessKeyboardAndTrackpad

## Installing the software{pagestep}

* Download Raspbian OpenFlexure from the [openflexure website](https://openflexure.org/projects/microscope/install)
* Follow the instructions on the website for installing it onto a [micro SD card]{qty:1, cat:electronic}
* Once it is installed, insert the SD card into the microscope

## Connecting up your microscope{pagestep}

* Connect a [Mini HDMI to HDMI cable]{qty:1, cat:electronic} and a [USB to micro USB cable]{qty:1, cat:electronic} between the Raspberry Pi and the screen.
* Connect a [USB-C power cable]{qty:1, cat:electronic} to the Raspberry Pi. If you want to use the microscope remotely connect the cable to a battery pack.
* Connect a [wireless keyboard with trackpad]{qty:1, cat:electronic} to the Raspberry Pi

## Power up your microscope{pagestep}

* Power up your microscope and open OpenFlexure Connect.
* Make sure you run the camera calibration in the OpenFlexure software.
* To adjust the magnification unscrew the M12 lens further.