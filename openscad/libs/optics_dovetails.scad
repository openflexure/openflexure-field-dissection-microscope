use <../../openflexure-microscope/openscad/libs/locking_dovetail.scad>
use <../../openflexure-microscope/openscad/libs/utilities.scad>
use <../../openflexure-microscope/openscad/libs/libdict.scad>
use <./rack_and_gear.scad>
use <./parameters.scad>
use <./interfaces.scad>

function camera_cable_clip_hole_sep() = 24;


module camera_cable_clip(){
    difference(){
        hull(){
            for(x_tr = [-0.5, 0.5]*camera_cable_clip_hole_sep()){
                translate_x(x_tr){
                    cylinder(d=4.5, h=3, $fn=16);
                }
            }
        }
        for(x_tr = [-0.5, 0.5]*camera_cable_clip_hole_sep()){
            translate_x(x_tr){
                cylinder(d=2.6, h=7, $fn=16, center=true);
            }
        }
        cube([18, 10, 3], center=true);
    }
}

function camera_cable_clip_pos(focus_dt_params) = let(
    height = key_lookup("overall_height", focus_dt_params),
    focus_dt_y = focus_dt_y(focus_dt_params)
) [0, focus_dt_y+6, height];

module optics_carriage(focus_dt_params, optics_dt_params, gearing_params){
    height = key_lookup("overall_height", focus_dt_params);
    clip_pos = camera_cable_clip_pos(focus_dt_params);
    difference(){
        focus_dovetail_m(focus_dt_params, optics_dt_params);
        optics_dovetail_f(optics_dt_params);
        rack_cutout(gearing_params, height);
        for(x_tr = [-0.5, 0.5]*camera_cable_clip_hole_sep()){
            translate(clip_pos + [x_tr, 0, -6]){
                no2_selftap_hole(h=7);
            }
        }
    }
    rack_for_carriage(gearing_params, height);

}

module rack_for_carriage(gearing_params, height){
    width = key_lookup("rack_width", gearing_params());
    thickness = key_lookup("thickness", gearing_params());

    translate_y(rack_y_pos()){
        intersection(){
            mirror([0, 1, 0]){
                rotate_y(-90){
                    rack(gearing_params);
                }
            }
            translate([-thickness, -width]){
                cube([2*thickness, 2*width, height]);
            }
        }
    }
}

module rack_cutout(gearing_params, height){
    pitch = key_lookup("circular_pitch", gearing_params());
    thickness = key_lookup("thickness", gearing_params());

    translate_y(rack_y_pos()){
        translate([-(thickness+1)/2, -pitch/2, -1]){
            cube([thickness+1, pitch, height]);
        }
    }
}


module focus_dovetail_m(focus_dt_params, optics_dt_params){
    height = key_lookup("overall_height", focus_dt_params);
    width = key_lookup("overall_width", focus_dt_params);
    joining_block_width = 2*female_point(focus_dt_params).x;
    focus_dt_y = focus_dt_y(focus_dt_params);

    //joining block
    translate([-joining_block_width/2, focus_dt_y-tiny()]){
        cube([joining_block_width, abs(focus_dt_y)+2*tiny(), height]);
    }

    translate_y(focus_dt_y){
        solid_male_dovetail(focus_dt_params);
    }
    hull(){
        translate_y(optics_dt_pos()){
            mirror([0, 1, 0]){
                dovetail_block(optics_dt_params);
            }
        }
        translate([-width/2, focus_dt_y]){
                cube([width, 4, height]);
        }
    }
}

module optics_dovetail_f(optics_dt_params){
    block_depth = key_lookup("block_depth", optics_dt_params);
    height = key_lookup("overall_height", optics_dt_params);
    translate_y(optics_dt_pos()+block_depth){
        translate_z(-1*tiny()){
            dovetail_f_cutout(optics_dt_params, height=height+1);
        }
    }
}

module pinion_housing(focus_dt_params, gearing_params){
    height = key_lookup("overall_height", focus_dt_params);

    difference(){
        translate_y(focus_dt_y(focus_dt_params)){
            difference(){
                dovetail_block(focus_dt_params);
                translate_z(-1*tiny()){
                    dovetail_f_cutout(focus_dt_params, height=height+1);
                }
            }
        }
        pinion_cutout(focus_dt_params, gearing_params);
    }
    pinion_housing_backplane(focus_dt_params);
}

function pinion_clearance(gearing_params) = let(
    pinion_clearance_d = gear_clearance_diameter(gearing_params),
    pinion_t = key_lookup("thickness", gearing_params)
) [pinion_clearance_d, pinion_clearance_d, pinion_t+.5];

module pinion_cutout(focus_dt_params, gearing_params){
    height = key_lookup("overall_height", focus_dt_params);
    width = key_lookup("overall_width", focus_dt_params);
    pitch = key_lookup("circular_pitch", gearing_params);
    pinion_clearance = pinion_clearance(gearing_params);
    rack_clearance = [pinion_clearance.z, pitch, 2*height+1];
    translate_y(rack_y_pos()){
        cube(rack_clearance, center=true);
    }
    main_gear_placement(gearing_params){
        cube(pinion_clearance, center=true);
        cylinder(d=12, h=99, center=true, $fn=32);
        reflect_z(){
            translate_z(width/2){
                cylinder(d=22.2, h=6, center=true, $fn=32);
            }
        }
    }
}

module pinion_housing_backplane(focus_dt_params){
    height = key_lookup("overall_height", focus_dt_params);
    backplane_block_size = [50, 8, 90];

    translate([0, backplane_y_pos()+backplane_block_size.y/2, height/2]){
        difference(){
            cube(backplane_block_size, center=true);
            backplane_hole_pattern(h=20);
        }
    }
}

module backplane_hole_pattern(h=99){
    for_each_backplane_screw(){
        rotate_x(90){
            cylinder(d=6.5, h=h, center=true, $fn=24);
        }
    }
}