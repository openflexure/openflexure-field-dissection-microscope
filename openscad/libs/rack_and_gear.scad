use <../../openflexure-microscope/openscad/libs/MCAD/involute_gears.scad>
use <../../openflexure-microscope/openscad/libs/utilities.scad>
use <../../openflexure-microscope/openscad/libs/libdict.scad>
use <./parameters.scad>

function pitch_diameter(params) = let(
        number_of_teeth = key_lookup("n_pinion_teeth", params),
        circular_pitch = key_lookup("circular_pitch", params)
    ) (circular_pitch*number_of_teeth)/PI;

function gear_clearance_diameter(params) = let(
        number_of_teeth = key_lookup("n_pinion_teeth", params),
        circular_pitch = key_lookup("circular_pitch", params),
        clearance = key_lookup("clearance", params),
        module_size = circular_pitch/PI
    ) module_size* (number_of_teeth + 2*(1+clearance));

function gear_inner_diameter(params) = let(
        number_of_teeth = key_lookup("n_pinion_teeth", params),
        circular_pitch = key_lookup("circular_pitch", params),
        clearance = key_lookup("clearance", params),
        module_size = circular_pitch/PI
    ) module_size* (number_of_teeth - 2*(1+clearance));

module herringbone_gear(params, bore_diameter=5){
    number_of_teeth = key_lookup("n_pinion_teeth", params);
    circular_pitch = key_lookup("circular_pitch", params);
    pressure_angle = key_lookup("pressure_angle", params);
    skew = key_lookup("skew", params);
    gear_thickness = key_lookup("thickness", params);
    hub_thickness = gear_thickness;
    rim_thickness = gear_thickness;
    hub_diameter=pitch_diameter(params)/2;
    clearance = key_lookup("clearance", params);

    //Note that MCAD circular pitch is not in mm but in mm Radians per degree
    mcad_circ_pitch = circular_pitch*180/PI;
    //calculate the twist as MCAD has it from the skew in units of the pitch
    twist = skew*360/number_of_teeth;

    // Note also clearance is set as a fraction of module size rather than an absolute clearance
    // as set in MCAD
    reflect_z(){
        gear(number_of_teeth=number_of_teeth,
             circular_pitch=mcad_circ_pitch,
             pressure_angle=pressure_angle,
             circles=0,
             gear_thickness=gear_thickness/2,
             hub_thickness=hub_thickness/2,
             rim_thickness=rim_thickness/2,
             hub_diameter=hub_diameter,
             bore_diameter=bore_diameter,
             twist=twist,
             clearance=clearance*circular_pitch/PI);
    }
}

//width is the distance to the back of thr rack from the pitch line
module rack(params){
    number_of_teeth = key_lookup("n_rack_teeth", params);
    width = key_lookup("rack_width", params);
    pitch = key_lookup("circular_pitch", params);
    pressure_angle = key_lookup("pressure_angle", params);
    skew = key_lookup("skew", params);
    thickness = key_lookup("thickness", params);
    clearance = key_lookup("clearance", params);
    module_size = pitch/PI;

    reflect_z(){
        for (i = [0:number_of_teeth-1]){
            translate([pitch*i,0,0]){
                rack_tooth(pitch=pitch,
                        pressure_angle=pressure_angle,
                        thickness=thickness/2,
                        skew=skew,
                        clearance=clearance);
            }
        }
    }

    translate([(-0.5-skew)*pitch, -width, -thickness/2]){
        cube([(number_of_teeth+skew)*pitch, width-module_size*(1+clearance), thickness]);
    }
}

module rack_tooth(pitch, pressure_angle, thickness, skew, clearance=0.25){

    module_size = pitch/PI;
    //top of rack tooth (Addendum), or height above reference line
    top = module_size;
    //bottom of rack tooth (Dedendum), or height below reference line
    bottom = -(1+clearance)*module_size;

    tooth_depth = (2+clearance)*module_size;
    tooth_working_depth = 2*module_size;
    //Note that the working slope is symmetric. It doesn't include the
    //extra clearence at the bottom of the tooth
    width_of_working_slope = tooth_working_depth*tan(pressure_angle);
    width_of_tip = pitch/2-width_of_working_slope;
    top_point_x = width_of_tip/2;
    bottom_point_x = width_of_tip/2 + tooth_depth*tan(pressure_angle);

    points = [[-bottom_point_x, bottom, 0],
              [bottom_point_x, bottom, 0],
              [top_point_x, top, 0],
              [-top_point_x, top, 0],
              [-bottom_point_x-skew*pitch, bottom, thickness],
              [bottom_point_x-skew*pitch, bottom, thickness],
              [top_point_x-skew*pitch, top, thickness],
              [-top_point_x-skew*pitch, top, thickness]
              ];
    faces = [[0,1,2,3],  // bottom
             [4,5,1,0],  // front
             [7,6,5,4],  // top
             [5,6,2,1],  // right
             [6,7,3,2],  // back
             [7,4,0,3]]; // left
    polyhedron(points=points, faces=faces);
}

module main_gear_placement(params, in_place=true){
    if (!in_place){
        children();
    }
    else {
        y_tr = rack_y_pos()-pitch_diameter(params)/2;
        translate([0, y_tr, pinion_z()]){
            rotate_y(90){
                children();
            }
        }
    }
}

module focus_pinion(params){
    thickness = key_lookup("thickness", params);
    bearing_id = key_lookup("bearing_id", params);
    id = pitch_diameter(params);
    difference(){
        intersection(){
            reflect_z(){
                cylinder(d1=id+2*(thickness/2+tiny()),
                        d2=id,
                        h=thickness/2+tiny());
            }
            herringbone_gear(params, bore_diameter=0);
        }
        //hexagonal hole 0.2 mm learger than bearing ID
        cylinder(d=bearing_id+0.2, h=99, center = true, $fn=6);
    }

}
