use <../../openflexure-microscope/openscad/libs/utilities.scad>
use <./parameters.scad>
use <./interfaces.scad>


module base_ring(){
    difference(){
        hull(){
            linear_extrude(base_riser_height()){
                projection(cut=false){
                    riser();
                }
            }
            cylinder(d1=150, d2=140, h=base_ring_height(), $fn=100);
        }
        hull(){
            cylinder(d=120, h=base_riser_height()*2, center=true, $fn=100);
            translate_y(100){
                cylinder(d=20, h=base_ring_height()*3, center=true, $fn=100);
            }
        }
        translate_y(riser_screw_pos().y){
            cylinder(d=30, h=(riser_screw_pos().z-0.4)*2, center=true);
        }
        translate(riser_screw_pos()){
            intersection(){
                cylinder(d=30, h=base_riser_height()*3, center=true);
                hole_from_bottom(r=6.5/2, h=base_riser_height(), base_w=30, big_bottom=false);
            }
        }
        riser_keys(clearance=0.2);
    }
}

module riser_keys(clearance=0){
    clearance_dims = [2, 2, 2]* clearance;
    reflect_x(){
        translate([riser_dims().x/3, backplane_y_pos()-riser_dims().y/2, base_riser_height()]){
            cube([7.5, riser_dims().y, 30]+clearance_dims, center=true);
        }
    }
}

//riser in printing position
module riser_stl(){

    riser_dims = riser_dims();
    y_tr = -backplane_y_pos()+riser_dims.y;
    z_tr = -base_riser_height()-riser_dims.z/2;
    rotate_x(90){
        translate([0, y_tr, z_tr]){
            riser();
        }
    }
}

module riser(){
    riser_nut_pos = [riser_screw_pos().x, riser_screw_pos().y, base_riser_height()+5];
    riser_dims = riser_dims();
    radius = 8;
    x_pos = riser_dims.x/2-radius;
    y_pos1 = backplane_y_pos()-radius;
    y_pos2 = backplane_y_pos()-riser_dims.y+radius;
    difference(){
        translate_z(base_riser_height()){
            hull(){
                for (cyl_pos = [[x_pos, y_pos1],
                                [-x_pos, y_pos1],
                                [x_pos, y_pos2],
                                [-x_pos, y_pos2]]){
                    translate(cyl_pos){
                        cylinder(r=radius, h=riser_dims.z, $fn=32);
                    }
                }
            }
        }

        for (hole = riser_top_holes()){
            translate(hole){
                mirror([0,0,1]){
                    m6_nut_slot_with_depession(angle=-90, shaft_above=20);
                }
            }
        }

        translate(riser_nut_pos){
            m6_nut_slot_with_depession(angle=-90, shaft_above=20);
        }
        translate([0, backplane_y_pos()-12, focus_assembly_z()]){
            for_left_backplane_screws(){
                ang = -90;
                rotate_z(-ang){
                    rotate_y(ang){
                        m6_nut_slot_with_depession(angle=90, shaft_above=10);
                    }
                }
            }
            for_right_backplane_screws(){
                ang = 90;
                rotate_z(-ang){
                    rotate_y(ang){
                        m6_nut_slot_with_depession(angle=90, shaft_above=10);
                    }
                }
            }
        }
    }
    riser_keys();
}

function riser_top_holes() = [for (i = [-15, 15], j = [-10, -riser_dims().y+10])
                                  [i, j+backplane_y_pos(), stand_top_z()-10]
                             ];


module m6_nut_slot(shaft_above=99, shaft_below=99, length=99, angle=0){
    //Max nut numensions for an M6
    width = 10;
    height = 5.2;
    clearance = 0.5;
    //corner to corner distance
    c2c_dist = 2/sqrt(3)*(width+clearance);
    rotate_z(angle){
        hull(){
            cylinder(d=c2c_dist, h= height+clearance, $fn=6);
            translate_x(length){
                cylinder(d=c2c_dist, h= height+clearance, $fn=6);
            }
        }
        if (shaft_above>0){
            translate_z(height+clearance+0.5-tiny()){
                rotate_z(90){
                    hole_from_bottom(r=6.5/2, h=shaft_above, base_w=width+clearance, big_bottom=false);
                }
            }
        }
        if (shaft_below>0){
            mirror([0,0,1]){
                rotate_z(90){
                    hole_from_bottom(r=6.5/2, h=shaft_below, base_w=width+clearance, big_bottom=false);
                }
            }
        }
    }
}

module m6_nut_slot_with_depession(shaft_above=99, shaft_below=99, length=99, angle=0){
    //Max nut numensions for an M6
    width = 10;
    height = 5.2;
    clearance = 0.5;
    interference = 0.8;
    //corner to corner distance
    c2c_dist_clear = 2/sqrt(3)*(width+clearance);
    c2c_dist_inter = 2/sqrt(3)*(width-interference);
    rotate(angle){
        translate_z(height){
            m6_nut_slot(shaft_above=shaft_above, shaft_below=0, length=length, angle=0);
        }
        hull(){
            translate_z(height){
                cylinder(d=c2c_dist_clear, h= height, $fn=6);
            }
            translate_z(height/2){
                cylinder(d=c2c_dist_inter, h=height, $fn=6);
            }
        }
        cylinder(d=c2c_dist_inter, h=height, $fn=6);
        if (shaft_below>0){
            mirror([0,0,1]){
                rotate_z(90){
                    hole_from_bottom(r=6.5/2, h=shaft_below, base_w= width-interference, big_bottom=false);
                }
            }
        }
    }
}

