use <../../openflexure-microscope/openscad/libs/utilities.scad>
use <../../openflexure-microscope/openscad/libs/libdict.scad>
use <../../openflexure-microscope/openscad/libs/locking_dovetail.scad>
use <../../openflexure-microscope/openscad/libs/cameras/camera.scad>

use <./parameters.scad>

function illumination_clip_hole_spacing() = 10;

module simple_camera_mount(optics_dt_params){
    optics_config = [["camera_type", "picamera_2"]];
    height = 20;
    mount_dt_params = replace_multiple_values([["overall_height", height],
                                               ["taper_block", true]],
                                              optics_dt_params);
    block_depth = key_lookup("block_depth", mount_dt_params);
    translate_y(optics_dt_pos()+block_depth){
        mirror([0, 1, 0]){
            dovetail_clamp_m(mount_dt_params);
        }
    }
    difference(){
        simple_camera_mount_body(mount_dt_params,
                                 optics_config,
                                 height,
                                 block_depth);
        mirror([0, 0, 1]){
            camera_transform() {
                camera_bottom_mounting_posts(optics_config, h=2.5, outers=false);
            }
        }
        translate_y(-14){
            cube([17, 2, 2*height+1], center=true);
        }

        hole_spacing = illumination_clip_hole_spacing();
        for(z_tr = [6, 6+hole_spacing]){
            translate([6, 6, z_tr]){
                rotate_y(90){
                    no2_selftap_hole(h=7);
                }
            }
        }
    }

}

module camera_transform() {
    rotate(-45){
        children();
    }
}


module simple_camera_mount_body(mount_dt_params,
                                optics_config,
                                height,
                                block_depth) {
    hull(){
        translate_y(optics_dt_pos()+block_depth){
            mirror([0, 1, 0]){
                linear_extrude(height){
                    back_of_block_2d(mount_dt_params);
                }
            }
        }
        translate_y(-14){
            camera_transform() {
                camera_bottom_mounting_posts(optics_config, h=height);
            }
        }
        camera_transform() {
            camera_bottom_mounting_posts(optics_config, h=height);
        }
    }
    mirror([0, 0, 1]){
        camera_transform() {
            camera_bottom_mounting_posts(optics_config, h=2.5, cutouts=false);
        }
    }
}

module illumination_clip_placement(){
    hole_spacing = illumination_clip_hole_spacing();
    translate([12.5, 6, hole_spacing/2+6]){
        rotate_y(90){
            children();
        }
    }
}

module illumination_clip(){
    hole_spacing = illumination_clip_hole_spacing();
    radius = 2;
    $fn=32;
    y_offset=-10;
    difference(){
        union(){
            hull(){
                for(x_tr = [-0.5, 0.5]*hole_spacing, y_tr = [0, y_offset]){
                    translate([x_tr, y_tr]){
                        cylinder(r=radius, h=4, center=true);
                    }
                }
            }
            translate_y(y_offset){
                rotate_y(90){
                    cylinder(h=2*radius+hole_spacing, r=6, center=true);
                }
            }
        }
        translate_y(y_offset){
            rotate_y(90){
                cylinder(h=2*radius+hole_spacing+1, r=3.5, center=true);
            }
        }
        mirror([0,0,1]){
            cylinder(d=99, h=20);
        }
        for(x_tr = [-0.5, 0.5]*hole_spacing){
            translate_x(x_tr){
                cylinder(d=2.5, h=7, center=true);
            }
        }
    }
}

function m12_optics_config(camera_type = "picamera_2") = let(
    config_dict = [["optics_type", "spacer"],
                   ["camera_type", camera_type],
                   ["lens_r", 12.5],
                   ["parfocal_distance", 6],
                   ["lens_h", 2.5],
                   ["lens_spacing", 6]]
) config_dict;


module m12_adapter(){
    optics_config = m12_optics_config();
    //This is the height of the block the camera mounts into.
    camera_mount_height = camera_mount_height(optics_config);

    // add the camera mount
    translate_z(camera_mount_height){
        difference(){
            camera_mount(optics_config, screwhole=false, counterbore=false);
            camera_mount_counterbore(optics_config);
            for (tr = [9,-9] ){
                angle = m12_mount_angle();
                translate([tr*sin(angle),tr*cos(angle),-2]){
                    no2_selftap_counterbore(flip_z=true);
                }
            }
            translate_z(-camera_mount_height){
                rotate(45){
                    cube([9, 10, 8], center=true);
                }
            }
        }
    }
}
