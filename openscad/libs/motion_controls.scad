use <../../openflexure-microscope/openscad/libs/utilities.scad>
use <../../openflexure-microscope/openscad/libs/libdict.scad>
use <parameters.scad>

module focus_handle(h=16){
    translate_z(-h/2){
        cylinder(d=12, h=h/2);
        translate_z(-h/2){
            difference(){
                cylinder(d=focus_knob_diameter(), h=h/2);
                for (angle = [30:30:360]){
                    rotate(angle){
                        translate_x(25){
                            cylinder(r=6, h=99, center=true);
                        }
                    }
                }
            }
        }
    }
}

function focus_shaft_separation() = 40;

module focus_knob(gear_params, shaft=true, on_printbed=false){
    bearing_id = key_lookup("bearing_id", gear_params);

    locking_depth = 3;
    locking_radius = bearing_id*sqrt(3)/2-0.2;
    //TODO: parameterise shaft length
    raised_length = shaft ? locking_depth : 0;
    shaft_length = shaft ? focus_shaft_separation()+locking_depth : locking_depth;
    handle_h = 16;
    z_shift = on_printbed ? handle_h +shaft_length/2 : 0;

    translate_z(z_shift){
        difference(){
            union(){
                translate_z(-raised_length/2){
                    cylinder(d=bearing_id+0.1, h=shaft_length-raised_length, center=true, $fn=6);
                }
                if (shaft){
                    translate_z(shaft_length/2-raised_length){
                        rotate_z(-30){
                            cylinder(d=locking_radius-0.2, h=raised_length, $fn=3);
                        }
                    }
                }
                translate_z(-shaft_length/2){
                    focus_handle(handle_h);
                }
            }
            if (shaft){
                translate_z(shaft_length/2){
                    no2_selftap_hole(h=14, center=true);
                }
            }
            else {
                rotate_z(-30){
                    cylinder(d=locking_radius, h=locking_depth+tiny(), center=true, $fn=3);
                }
                translate_z(-3){
                    no2_selftap_counterbore(flip_z=true);
                }
            }
        }
    }
}