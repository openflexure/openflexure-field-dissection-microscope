use <../../openflexure-microscope/openscad/libs/lib_microscope_stand.scad>
use <../../openflexure-microscope/openscad/libs/utilities.scad>
use <./parameters.scad>
use <./stand.scad>


module position_pi_tray_stl(){
    base_size = electronics_drawer_base_size();
    y_tr = backplane_y_pos()+base_size.y+1;
    translate([-base_size.x/2, y_tr, stand_top_z()])
    rotate_x(180){
        children();
    }
}

module pi_tray(){
    position_pi_tray_stl(){
        pi_tray_stl();
    }
}

module pi_tray_stl(){
    electronics_drawer_h = 30;
    base_size = electronics_drawer_base_size();
    wall_t = electronics_drawer_wall_t();
    standoff_h = electronics_drawer_standoff_h();
    hole_pos = pi_hole_pos(true);


    difference(){
        union(){
            cube([base_size.x, wall_t, electronics_drawer_h]);
            translate(electronics_drawer_front_pos()){
                cube([wall_t, base_size.y, electronics_drawer_h]);
            }
        }

        pi_connector_holes(pi_version=4);
    }

    difference(){
        union(){
            cube(base_size);
            // standoffs for pi
            for (hole = hole_pos){
                translate(hole){
                    cylinder(d=5.5, h=standoff_h, $fn=12);
                }
            }
            pi_tray_lugs(base_size);
        }
        pi_tap_holes(connector_side=true);
        translate_y(base_size.y/2){
            cube(25, center=true);
        }
    }
}

// Returns an array of 4 mounting positions. Each position is two x-y positions.
// The first is the hole poisition. The second is the relative position of the
// other side of the lug
function pi_tray_mounts(base_size) = [[[base_size.x-3,-3], [0, 5]],
                                      [[-3, 3], [5, 0]],
                                      [[-3, base_size.y-3], [5, 0]],
                                      [[base_size.x-3, base_size.y+3], [0, -5]]];

module pi_tray_lugs(base_size){
    mounts = pi_tray_mounts(base_size);
    for (mount = mounts){
        mount_hole = mount[0];
        back_of_lug = mount[1];
        translate(mount_hole){
            difference(){
                hull(){
                    cylinder(d=5.5, h=base_size.z, $fn=12);
                    translate(back_of_lug){
                        cylinder(d=5.5, h=base_size.z, $fn=12);
                    }
                }
                cylinder(d=2.6, h=3*base_size.z, $fn=12, center=true);
            }
        }
    }
}

module electronics_mounting_plate_stl(){
    translate_z(-stand_top_z()){
        electronics_mounting_plate();
    }
}

function electronics_mounting_plate_thickness() = 4;

module electronics_mounting_plate(){
    thickness = electronics_mounting_plate_thickness();
    difference(){
        translate_z(stand_top_z()){
            linear_extrude(thickness){
                hull(){
                    projection(){
                        riser();
                        pi_tray();
                    }
                }
            }
        }
        electronics_mounting_plate_holes();
    }
}
module electronics_mounting_plate_holes(){
    base_size = electronics_drawer_base_size();
    mounts = pi_tray_mounts(base_size);
    thickness = electronics_mounting_plate_thickness();
    position_pi_tray_stl(){
        for (mount = mounts){
            mount_hole = mount[0];
            translate(mount_hole){
                no2_selftap_hole(h=3*thickness, center=true);
            }
        }
    }
    for (hole = riser_top_holes()){
        translate(hole){
            cylinder(d=6.6, h=25, $fn=16);
        }
    }
}

function screen_mount_position(screen_dims) = let(
    mount_h = electronics_mounting_plate_thickness() + stand_top_z()
) [0, backplane_y_pos()+screen_dims.y/3, mount_h];

module at_screen_mounting_loc(screen_dims, excess, each_screw=true, rel_to_mount=true){
    tr = rel_to_mount ? [0, 0, 0] : screen_mount_position(screen_dims);
    translate(tr){
        rotate_x(-45){
            y_offset = -(screen_dims.y+3*excess)/2;
            if (each_screw){
                for (i=[-0.5, 0.5]*157, j = [-0.5, 0.5]*116){
                    translate([i, j+y_offset]){
                        children();
                    }
                }
            }
            else{
                translate_y(y_offset){
                    children();
                }
            }
        }
    }
}

module screen_mount(){
    screen_dims = standard_screen_size();
    excess = screen_bevel();
    mount_pos = screen_mount_position(screen_dims);
    difference(){
        translate(mount_pos){
            screen_mount_body(screen_dims, excess);
            at_screen_mounting_loc(screen_dims, excess){
                cylinder(d=10, h=12);
            }
        }
        translate(mount_pos){
            at_screen_mounting_loc(screen_dims, excess){
                cylinder(d=4, h=excess+10, $fn=12, center=true);
            }
            at_screen_mounting_loc(screen_dims, excess){
                translate_z(2){
                    mirror([0,0,1]){
                        cylinder(d=6.6, h=10, $fn=6);
                    }
                }
            }
        }
        for (hole = riser_top_holes()){
            translate(hole){
                cylinder(d=6.6, h=25, $fn=16);
            }
        }
    }
}

module screen_mount_body(screen_dims, excess){
    difference(){
        screen_mount_body_shape(screen_dims, excess);
        translate([0, -excess*1.4, excess/2]){
            screen_mount_body_shape(screen_dims-[excess,0], excess);
        }
    }

}

module screen_mount_body_shape(screen_dims, excess){
    hull(){
        screen_plate(screen_dims, excess);
        linear_extrude(1){
            projection(){
                screen_plate(screen_dims, excess);
            }
        }
    }
}

module screen_plate(screen_dims, excess){
    plate_dims = screen_dims+[1, 2]*excess;
    rotate_x(-45){
        translate([-plate_dims.x/2, -plate_dims.y]){
            cube([plate_dims.x, plate_dims.y, 5]);
        }
    }
}