
function backplane_screws_x_pos() = [-12.5, 12.5];
function backplane_screws_z_pos() = [-37.5, 37.5];

module for_each_backplane_screw(){
    x_positions = backplane_screws_x_pos();
    z_positions = backplane_screws_z_pos();
    for (i = x_positions, j = z_positions){
        translate([i, 0, j]){
            children();
        }
    }
}

module for_left_backplane_screws(){
    x_pos = backplane_screws_x_pos()[0];
    z_positions = backplane_screws_z_pos();
    for (i = x_pos, j = z_positions){
        translate([i, 0, j]){
            children();
        }
    }
}

module for_right_backplane_screws(){
    x_pos = backplane_screws_x_pos()[1];
    z_positions = backplane_screws_z_pos();
    for (i = x_pos, j = z_positions){
        translate([i, 0, j]){
            children();
        }
    }
}