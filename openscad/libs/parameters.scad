
use <../../openflexure-microscope/openscad/libs/illumination.scad>
use <../../openflexure-microscope/openscad/libs/locking_dovetail.scad>
use <../../openflexure-microscope/openscad/libs/libdict.scad>

function m12_mount_angle() = 13;

function backplane_y_pos() = -120;

function gearing_params() = [["circular_pitch", 6],
                             ["pressure_angle", 20],
                             ["skew", 0.5],
                             ["thickness", 10],
                             ["clearance", 0.3],
                             ["rack_width", 10],
                             ["n_pinion_teeth", 16],
                             ["n_rack_teeth", 12],
                             ["bearing_id", 8],
                             ["bearing_od", 22],
                             ["bearing_t", 7]
                            ];

// Optics use same mounting dovetail as OpenFlexure
// Microscope illumination
function optics_dt_params() = replace_value("overall_height",
                                            40,
                                            illumination_dt_params());

function optics_carriage_height() = 60;

function optics_male_dt_params() = replace_value("overall_height",
                                                 optics_carriage_height(),
                                                 illumination_dt_params());

function optics_dt_pos() = -50;

//y position of the rack reference line (centre of working area of teeth)
function rack_y_pos() = optics_dt_pos()-17;
function pinion_z() = key_lookup("overall_height", focus_dt_params())/2;

function focus_dt_shift() = 1;

function focus_dt_params() = let(
    depth=6,
    block_depth = rack_y_pos()+depth+focus_dt_shift()-backplane_y_pos()-1
) dovetail_params(overall_height=40,
                  overall_width=35,
                  depth=depth,
                  block_depth=block_depth);

function focus_female_dt_params() = replace_value("overall_height",
                                                  optics_carriage_height(),
                                                  focus_dt_params());

function focus_dt_y(focus_dt_params) = rack_y_pos() + focus_dt_shift() + key_lookup("depth", focus_dt_params);

function focus_assembly_z() = 150;

function focus_knob_diameter() = 40;

function base_ring_height() = 14;
function base_riser_height() = base_ring_height()*3;
function riser_dims() = [70,40,200];

function stand_top_z() = riser_dims().z + base_riser_height();

function riser_screw_pos() = [0, backplane_y_pos()-riser_dims().y/2, base_riser_height()-5];

function standard_screen_size() = [165, 125];
function screen_bevel() = 8;

