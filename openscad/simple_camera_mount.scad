use <./libs/optics_mounts.scad>
use <./libs/parameters.scad>

simple_camera_mount_stl(optics_dt_params(), in_place=false);

module simple_camera_mount_stl(optics_dt_params, in_place=true){
    if (in_place){
        simple_camera_mount(optics_dt_params);
    }
    else{
        rotate([180, 0, 0]){
            simple_camera_mount(optics_dt_params);
        }
    }
}