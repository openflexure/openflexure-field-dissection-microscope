use <libs/rack_and_gear.scad>
use <libs/parameters.scad>

main_gear(gearing_params(), in_place=false);

module main_gear(params, in_place=true){
    main_gear_placement(params, in_place){
        focus_pinion(params);
    }
}