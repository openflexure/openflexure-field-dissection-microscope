use <../openflexure-microscope/openscad/libs/utilities.scad>
use <./libs/motion_controls.scad>
use <libs/parameters.scad>

focus_knob(gearing_params(), shaft=false, on_printbed=true);
translate_x(focus_knob_diameter()+3){
    focus_knob(gearing_params(), shaft=true, on_printbed=true);
}

