use <./libs/parameters.scad>
use <./libs/optics_dovetails.scad>


rotate([-90,0,0]){
    optics_carriage(focus_female_dt_params(), optics_male_dt_params(), gearing_params());
}
